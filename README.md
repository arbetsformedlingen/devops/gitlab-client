# NAME

gitlab-client - naive wrapper around a gitlab library

# SYNOPSIS

gitlab-client \[options\] create [ webhook | repo ]
gitlab-client \[options\] search [ repo ]

    General options:
    -e, --endpoint ENDPOINT          the gitlab api url
    -V, --visibility VISIBILITY      repo visibility: public private internal
    -h, --help                       show this text

    Options for create and search repo:
    -n, --name NAME                  the name of the repo
    -g, --group-id GROUP-ID          the numeric group ID to create the repo in

    Options for create webhook:
    -p, --project-id PROJECT-ID      the project ID to set webhook in
    -P, --push-hook                  set a project push hook
    -m, --merge-hook                 set a project merge hook
        --tag-hook                   set a project tag hook
    -u, --url URL                    webhook url
    -t, --token TOKEN                webhook token


# BUILD AND INSTALLATION

## Local environment
Install ruby and run `gem install gitlab`.

Then you should be able to run `gitlab-client.rb`.

## Containerised environment
Run `make build`.

See the `Makefile` rule `run` for a typical example of running the program containerised.

# CONFIGURATION

These environment variables should be set at runtime:
 -  GITLAB\_API\_ENDPOINT  (typically `https://gitlab.com/api/v4`)
 -  GITLAB\_API\_PRIVATE_TOKEN

# COPYRIGHT

License GPLv3+: GNU GPL version 3
or later &lt;https://gnu.org/licenses/gpl.html>.  This is free software:
you are free to change and re distribute it. There is NO WARRANTY, to
the extent permitted by law.

This program uses the [https://narkoz.github.io/gitlab/](gitlab Ruby wrapper and CLI for GitLab API)
This library is available under the [https://github.com/NARKOZ/gitlab/blob/master/LICENSE.txt](BSD-2-Clause License).
