# GNU Make makes multiline shell blocks possible
.ONESHELL:
SHELL = /bin/bash
CONTAINERCMD = $(shell which podman || which docker)


IMAGENAME = gitlab-client



all: build run


build:
	@if ! $(CONTAINERCMD) image inspect $(IMAGENAME) >/dev/null 2>&1; then
		$(CONTAINERCMD) build -t $(IMAGENAME) . >&2
	fi


# this rule is provided as an example
run: build
	# for convenience, source WEBHOOK_URL and WEBHOOK_TOKEN from a file - but you can set it otherwise too of course
	. secrets.sh
	PROJNAME=<nice project name>
	GROUPID=<numeric gitlab group ID>
	$(CONTAINERCMD) run --env-file secrets.sh --rm -i $(IMAGENAME) --name $$PROJNAME --group-id $$GROUPID create repo | tee /tmp/gitlab_repo.$(USER)
	PROJID=$$(cat /tmp/gitlab_repo.$(USER) | jq -r .projectid)
	$(CONTAINERCMD) run --env-file secrets.sh --rm -i $(IMAGENAME) --project-id $$PROJID --push-hook --merge-hook --tag-hook --url "$$WEBHOOK_URL" --token "$$WEBHOOK_TOKEN" create webhook


clean:
	if $(CONTAINERCMD) image inspect $(IMAGENAME) >/dev/null; then
		$(CONTAINERCMD) rmi $(IMAGENAME) >&2
	fi
