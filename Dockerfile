FROM docker.io/debian:11-slim

RUN mkdir /app

COPY gitlab-client.rb /app/gitlab-client

RUN chmod a+rx /app/gitlab-client \
    && apt-get -y update \
    && apt-get -y --no-install-recommends install ruby \
    && apt-get -y autoremove --purge \
    && apt-get -y clean \
    && rm -rf /var/lib/apt/lists/* \
    && gem install gitlab

ENTRYPOINT [ "/app/gitlab-client" ]
