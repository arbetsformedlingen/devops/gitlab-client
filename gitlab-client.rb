#!/usr/bin/env ruby
require 'gitlab'
require 'optparse'
require 'ostruct'

require 'net/http'
require 'uri'


# These environment variables should be set:
#    GITLAB_API_ENDPOINT
#    GITLAB_API_PRIVATE_TOKEN


### Handle cli args
options = OpenStruct.new(:endpoint => "https://gitlab.com/api/v4",
                         :visibility => "private")
OptionParser.new do |opt|
  opt.on('-e', '--endpoint ENDPOINT', 'the gitlab api url') { |o| options[:endpoint] = o }
  opt.on('-V', '--visibility VISIBILITY', 'repo visibility: public private internal') { |o| options[:visibility] = o }
  opt.on('-n', '--name NAME', 'the name of the repo') { |o| options[:name] = o }
  opt.on('-g', '--group-id GROUP-ID', 'the numeric group ID to create the repo in') { |o| options[:groupid] = o }
  opt.on('-p', '--project-id PROJECT-ID', 'the project ID to set webhook in') { |o| options[:projectid] = o }
  opt.on('-P', '--push-hook', 'set a project push hook') { |o| options[:webhook_push] = o }
  opt.on('-m', '--merge-hook', 'set a project merge hook') { |o| options[:webhook_merge] = o }
  opt.on('-t', '--tag-hook', 'set a project tag hook') { |o| options[:webhook_tag] = o }
  opt.on('-u', '--url URL', 'webhook url') { |o| options[:url] = o }
  opt.on('-t', '--token TOKEN', 'webhook token') { |o| options[:token] = o }
end.parse!

# what to do with the object: create
verb = ARGV.at(0)
verbs = ["create", "search" ]
verbs.detect { |el|  el == verb } || abort("verb #{verb} should be one of: " + verbs.join(" ") )

# the object to work in: repo | webhook
object = ARGV.at(1)
objects = ["repo", "webhook"]
objects.detect { |el|  el == object } || abort("object should be one of: " + objects.join(" ") )

# allowed visibility
visibilities = ["public", "private", "internal" ]
visibilities.detect { |el|  el == options.visibility } || abort("visibility should be one of: " + visibilities.join(" ") )


# method to make PUT calls, to edit objects in Gitlab
def apicall_put(token, url, options)
  uri = URI.parse(url)
  request = Net::HTTP::Put.new(uri)
  request.content_type = "application/json"
  request["Private-Token"] = token
  request.set_form_data(options)
  req_options = {
    use_ssl: uri.scheme == "https",
  }

  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)
  end
end



client = Gitlab.client(endpoint: options.endpoint) #private_token: private_token


if object == "repo"
  if verb == "create"
    project = client.create_project(options.name, namespace_id: options.groupid, visibility: options.visibility)
    puts "{ \"projectid\": \"#{project.id}\", \
            \"ssh_url_to_repo\": \"#{project.ssh_url_to_repo}\", \
            \"http_url_to_repo\": \"#{project.http_url_to_repo}\" }"
  end
  if verb == "search"
    result = client.project_search(options.name);
    print result.select{|el| el.name.eql? options.name}
            .map{|el| "{ \"name\": \"#{el.name}\", \"id\": \"#{el.id}\" }" }.join("\n"),"\n"
  end
end


if object == "webhook"
  options.url != "" || abort("supply URL to webhook")

  if verb == "create"
    opt = {}

    if options[:webhook_push]
      opt.store(:push_events, 1)
    end
    if options.webhook_merge
      opt.store(:merge_requests_events, 1)
    end
    if options.webhook_tag
      opt.store(:tag_push_events, 1)
    end

    project = client.add_project_hook(options.projectid, options.url, opt)
    id = project.id

    puts "{ \"hookid\": \"#{id}\" }"

    # stupid library won't allow us to send the webhook token, so we make a straight
    # https call to the API instead
    apicall_put(ENV["GITLAB_API_PRIVATE_TOKEN"],
                ENV["GITLAB_API_ENDPOINT"]+"/projects/#{options.projectid}/hooks/#{id}",
                { :token => options.token, :url => options.url })
  end
end
